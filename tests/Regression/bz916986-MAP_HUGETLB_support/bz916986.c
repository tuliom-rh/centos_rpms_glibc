#include <sys/mman.h>
#ifndef MAP_HUGETLB
#error MAP_HUGETLB should be defined.
#endif
#ifndef MAP_STACK
#error MAP_STACK should be defined.
#endif
